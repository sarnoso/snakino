snakino
=======

Snake game for Arduino

Hardware:
* LCD 16x2
* 4 switches

Forked from [https://www.youtube.com/watch?v=IkTK6WMlMs4](https://www.youtube.com/watch?v=IkTK6WMlMs4)

New stuff:
* Multilevel in a single game
* No need of keypad, just simple switches
